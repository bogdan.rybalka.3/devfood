variable "project_id" {
  type = string
}

variable "env" {
  type = string
  default = "dev"
}

variable "credentials_file" {
  default = "~/GCP_CREDS.json"
  type = string
}

variable "region" {
  default = "europe-west3"
  type = string
}

variable "oauth_client_id" {
  type = string
  default = "1012016747272-a9u53gk9ln2hs9m6m5bgdfb7afnvaigc.apps.googleusercontent.com"
}
variable "oauth_client_secret" {
  type = string
  default = "D_WFr8QZ8_632Bk8Lf_60O5B"
}