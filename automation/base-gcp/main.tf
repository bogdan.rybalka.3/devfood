provider "google" {
  project = var.project_id
  region = var.region
  credentials = file(var.credentials_file)
}

resource "google_storage_bucket" "devfood-functions-bucket" {
  name = "devfood-functions-bucket-${var.env}"
  location = "EU"
  force_destroy = true
}

module "network" {
  source = "./modules/network"
}

module "idp" {
  source = "./modules/identityPlatform"
  oauth_client_id = var.oauth_client_id
  oauth_client_secret = var.oauth_client_secret
  functions_bucket_name = google_storage_bucket.devfood-functions-bucket.name
  project_id = var.project_id
  env = var.env
}