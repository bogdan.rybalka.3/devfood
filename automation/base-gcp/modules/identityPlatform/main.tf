resource "google_identity_platform_default_supported_idp_config" "idp_config" {
  enabled = true
  idp_id = "google.com"
  client_id = var.oauth_client_id
  client_secret = var.oauth_client_secret
}

resource "google_storage_bucket_object" "df-custom-claims-func-archive" {
  name = "df-custom-claims-func.zip"
  bucket = var.functions_bucket_name
  source = "${path.root}/../../functions/df-custom-claims-func/dist/df-custom-claims-func.zip"
}

resource "google_cloudfunctions_function" "df-custom-claims-func" {
  name = "df-custom-claims-func-${var.env}"
  runtime = "nodejs14"

  available_memory_mb = 128
  source_archive_bucket = var.functions_bucket_name
  source_archive_object = google_storage_bucket_object.df-custom-claims-func-archive.name
  entry_point = "processSignUp"
  event_trigger {
    event_type = "providers/firebase.auth/eventTypes/user.create"
    resource = "projects/${var.project_id}"
  }

}