variable "oauth_client_id" {
  type = string
}
variable "oauth_client_secret" {
  type = string
}
variable "functions_bucket_name" {
  type = string
}

variable "project_id" {
  type = string
}

variable "env" {
  type = string
}