resource "google_compute_network" "devfood-vpc" {
  name = "devfood-vpc"
  auto_create_subnetworks = false
  routing_mode = "REGIONAL"
}

resource "google_compute_subnetwork" "europe-3-main-subnet" {
  name = "europe-3-main-subnet"
  ip_cidr_range = "10.128.0.0/9"
  network = google_compute_network.devfood-vpc.id
  region = "europe-west3"
}

output "vpc_name" {
  value = google_compute_network.devfood-vpc.name
}
output "europe-west3-subnet" {
  value = google_compute_subnetwork.europe-3-main-subnet.name
}