variable "region" {
  default = "europe-west3"
  type = string
}

variable "k8s-node-count" {
  default = 3
  type = number
}

variable "k8s-machine-type" {
  default = "e2-medium"
  type = string
}

variable "network" {
  type = string
}

variable "subnetwork" {
  type = string
}