resource "google_container_cluster" "k8s-cluster" {
  name = "${var.region}-cluster"
  location = var.region
  network = var.network
  subnetwork = var.subnetwork

  remove_default_node_pool = true
  initial_node_count = 1
}

resource "google_container_node_pool" "primary-cluster-node-pool" {
  cluster = google_container_cluster.k8s-cluster.name
  location = var.region
  node_count = var.k8s-node-count

  node_config {
    preemptible = false
    machine_type = var.k8s-machine-type
  }
}