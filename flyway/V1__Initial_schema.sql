-- notes
-- 1. schedule history ~> get all schedules where :client_id sort by start_date
-- 2. deliverer is nullable in order table since one may have yet to be assigned after order creation
-- 3. client and user are two distinct roles since their use and purposes diverge

create schema devfood;
set search_path to devfood;

-- general
create table if not exists zone (
  id serial primary key,
  address varchar not null
);

create table if not exists meal_photo (
    id serial primary key,
    url varchar not null
);

create type auth_source as enum ('GOOGLE', 'PHONE', 'FACEBOOK');

create table if not exists client (
    id serial primary key,
    phone_number varchar(15) not null,
    first_name varchar not null,
    last_name varchar not null,
    auth_source auth_source not null,
    external_id varchar not null
);

create table if not exists deliverer (
    id serial primary key,
    phone_number varchar(15),
    first_name varchar not null,
    last_name varchar not null
);

create table if not exists delivery_point (
    id serial primary key,
    address_details varchar not null,
    geo_loc point not null,
    client_id int not null references client (id)
);

-- rationing
create type meal_status as enum ('AVAILABLE', 'TEMPORARILY_UNAVAILABLE', 'UNAVAILABLE');

create table if not exists meal (
    id serial primary key,
    name varchar not null,
    calories int,
    protein int,
    fat int,
    description text,
    portion_size float not null,
    meal_status meal_status not null,
    zone_id int not null references zone (id),
    photo_id int not null references meal_photo (id)
);

create table if not exists ration (
    id serial primary key,
    name varchar not null,
    description text,
    zone_id int not null references zone (id),
    photo_id int references meal_photo (id)
);

create table if not exists meal_to_ration (
    meal_id int not null references meal (id),
    ration_id int not null references ration (id),
    primary key (meal_id, ration_id)
);

-- orders
create table if not exists schedule (
    id serial primary key,
    start_date timestamptz not null,
    client_id int not null references client (id)
    -- day capacity?
    -- status?
);

create type order_status as enum ('CREATED', 'DONE', 'PENDING', 'CANCELLED');

create table if not exists meal_order (
    id serial primary key,
    ration_id int not null references ration (id),
    target_date timestamptz not null,
    delivery_point_id int not null references delivery_point (id),
    deliverer_id int references deliverer (id),
    zone_id int not null references zone (id),
    schedule_id int not null references schedule (id),
    status order_status not null
);

-- tags
create table if not exists tag (
    id serial primary key,
    name varchar not null unique
);

create table if not exists meals_to_tags (
    meal_id int not null references meal (id),
    tag_id int not null references tag (id),
    primary key (meal_id, tag_id)
);

create table if not exists rations_to_tags (
    ration_id int not null references ration (id),
    tag_id int not null references tag (id),
    primary key (ration_id, tag_id)
);
