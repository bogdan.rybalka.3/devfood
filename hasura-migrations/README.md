### Hasura migrations
Hasura's migration tool `hasura-cli` is concerned with both migrating tables of data database and with managing its own state which is referred as hasura metadata.
We dont care about the first one as its assumed by flyway, thus we manage only metadata which is stored in hasura-migrations/metadata subdirectory.

1. https://hasura.io/docs/latest/graphql/core/migrations/manage-metadata.html

TL:DR;

cd hasura-migrations

1. hasura metadata export - exports current metadata to files
2. hasura metadata apply - apply metadata migration
3. hasura metadata reload - make it visible