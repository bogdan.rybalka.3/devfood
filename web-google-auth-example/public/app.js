let firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDiFpPz47rELQ1CO0R1-5YI5ghcdAJ8bUA",
    authDomain: "hallowed-oven-324921.firebaseapp.com",
    projectId: "devfood-test"
});

var ui = new firebaseui.auth.AuthUI(firebase.auth());

ui.start('#firebaseui-auth-container', {
    callbacks: {
        signInSuccess: (currentUser, credential, redirectUrl) => {
            console.log(currentUser)
            console.log(credential)
            console.log(redirectUrl)
            return false;
        }
    },
    signInOptions: [
        {
            provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            scopes: [
            ],
            customParameters: {
                prompt: 'select_account'
            }
        }
    ]
});
