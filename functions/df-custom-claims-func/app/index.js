const admin = require('firebase-admin');
admin.initializeApp();

exports.processSignUp = (user, context) => {
    let customClaims;
    const adminMails = ['bogdan.rybalka.3@gmail.com', 'mykyta.levashov@scalac.io']
    if (user.email && adminMails.includes(user.email)) {
        customClaims = {
            'https://hasura.io/jwt/claims': {
                'x-hasura-default-role': 'admin',
                'x-hasura-allowed-roles': ['user', 'admin'],
                'x-hasura-user-id': user.uid
            }
        };
    } else {
        customClaims = {
            'https://hasura.io/jwt/claims': {
                'x-hasura-default-role': 'user',
                'x-hasura-allowed-roles': ['user'],
                'x-hasura-user-id': user.uid
            }
        };
    }
    return admin.auth().setCustomUserClaims(user.uid, customClaims);
}
