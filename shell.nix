with import <nixpkgs> {};

mkShell {
  buildInputs = [
    terraform
    google-cloud-sdk
    hasura-cli
    flyway
    nodejs
  ];

  # needed for hasura migrations
  shellHook = ''
      export HASURA_GRAPHQL_ADMIN_SECRET=blah
      alias happly='hasura metadata apply'
      alias hreload='hasura metadata reload'
    '';
}
